
# Rebuild of Top-popcon Debian GNU/Linux 12.x bookworm on amd64

This project rebuilds
[Top-popcon Debian GNU/Linux 12.x bookworm](https://www.debian.org/releases/bookworm/) on
amd64 in a GitLab pipeline, and publish diffoscope
output for any differences.

## Status

We have rebuilt **36%** of
**Top-popcon Debian GNU/Linux 12.x bookworm**!  That is **36%** of the
packages we have built, and we have built **100%** or
**50** of the **50** source packages to rebuild.

Top-popcon Debian GNU/Linux 12.x bookworm (on amd64) contains binary packages
that corresponds to **50** source packages.  Some binary
packages exists in more than one version, so there is a total of
**50** source packages to rebuild.  Of these we have identical
rebuilds for **18** out of the **50**
builds so far.

We have build logs for **50** rebuilds, which may exceed the
number of total source packages to rebuild when a particular source
package (or source package version) has been removed from the archive.
Of the packages we built, **18** packages could be rebuilt
identically, and there are **30** packages that we could
not rebuilt identically.  Building **2** package had build
failures.  We do not attempt to build **0** packages.

[[_TOC_]]

### Rebuildable packages

The following **18** packages could be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| acl | 2.3.1-3 | [build Mon Jul  8 23:54:14 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/acl/2.3.1-3/buildlog.txt) | [job 7291796454](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291796454) |
| adduser | 3.134 | [build Mon Jul  8 23:58:32 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/adduser/3.134/buildlog.txt) | [job 7291804970](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291804970) |
| apt | 2.6.1 | [build Tue Jul  9 19:22:42 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/apt/2.6.1/buildlog.txt) | [job 7301200833](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301200833) |
| base-files | 12.4+deb12u6 | [build Mon Jul  8 23:28:19 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/base-files/12.4+deb12u6/buildlog.txt) | [job 7291716077](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716077) |
| debconf | 1.5.82 | [build Mon Jul  8 23:40:35 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/debconf/1.5.82/buildlog.txt) | [job 7291760254](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760254) |
| debian-archive-keyring | 2023.3+deb12u1 | [build Tue Jul  9 19:23:04 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/debian-archive-keyring/2023.3+deb12u1/buildlog.txt) | [job 7301200835](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301200835) |
| dpkg | 1.21.22 | [build Mon Jul  8 23:23:40 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/dpkg/1.21.22/buildlog.txt) | [job 7291701586](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701586) |
| glibc | 2.36-9+deb12u7 | [build Mon Jul  8 23:58:02 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/glibc/2.36-9+deb12u7/buildlog.txt) | [job 7291804972](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291804972) |
| grep | 3.8-5 | [build Tue Jul  9 00:36:04 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/grep/3.8-5/buildlog.txt) | [job 7291938485](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291938485) |
| ncurses | 6.4-4 | [build Mon Jul  8 23:44:55 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/ncurses/6.4-4/buildlog.txt) | [job 7291771525](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291771525) |
| netbase | 6.4 | [build Mon Jul  8 23:37:43 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/netbase/6.4/buildlog.txt) | [job 7291752903](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752903) |
| pam | 1.5.2-6+deb12u1 | [build Mon Jul  8 23:27:59 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/pam/1.5.2-6+deb12u1/buildlog.txt) | [job 7291716075](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716075) |
| readline | 8.2-1.3 | [build Mon Jul  8 23:44:39 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/readline/8.2-1.3/buildlog.txt) | [job 7291771526](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291771526) |
| slang2 | 2.3.3-3 | [build Mon Jul  8 23:53:55 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/slang2/2.3.3-3/buildlog.txt) | [job 7291796491](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291796491) |
| sysvinit | 3.06-4 | [build Mon Jul  8 23:16:18 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/sysvinit/3.06-4/buildlog.txt) | [job 7291666007](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666007) |
| tzdata | 2024a-0+deb12u1 | [build Mon Jul  8 23:28:16 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/tzdata/2024a-0+deb12u1/buildlog.txt) | [job 7291716078](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716078) |
| ucf | 3.0043+nmu1 | [build Tue Jul  9 00:57:58 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/ucf/3.0043+nmu1/buildlog.txt) | [job 7291991376](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291991376) |
| util-linux | 2.38.1-5+deb12u1 | [build Mon Jul  8 23:23:35 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/util-linux/2.38.1-5+deb12u1/buildlog.txt) | [job 7291701587](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701587) |

### Build failures

The following **2** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| coreutils | 9.1-1 | [build Mon Jul  8 23:15:43 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/coreutils/9.1-1/buildlog.txt) | [job 7291666002](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666002) |
| tar | 1.34+dfsg-1.2+deb12u1 | [build Mon Jul  8 23:44:48 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/tar/1.34+dfsg-1.2+deb12u1/buildlog.txt) | [job 7291771524](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291771524) |

### Unrebuildable packages

The following **30** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and help us improve things!

| Package | Version | Build log | Jobs | Diffoscope |
| ------- | ------- | --------- | ---- | ---------- |
| attr | 1:2.5.1-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/attr/1:2.5.1-4/buildlog.txt) | build [7291752901](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752901) <br> diff [7291752909](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752909) | [diff 7291752909](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291752909/artifacts/diffoscope/index.html) |
| base-passwd | 3.6.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/base-passwd/3.6.1/buildlog.txt) | build [7294445399](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7294445399) <br> diff [7294445417](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7294445417) | [diff 7294445417](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7294445417/artifacts/diffoscope/index.html) |
| bash | 5.2.15-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/bash/5.2.15-2/buildlog.txt) | build [7301200834](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301200834) <br> diff [7301200849](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301200849) | [diff 7301200849](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7301200849/artifacts/diffoscope/index.html) |
| bzip2 | 1.0.8-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/bzip2/1.0.8-5/buildlog.txt) | build [7291752900](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752900) <br> diff [7291752907](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752907) | [diff 7291752907](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291752907/artifacts/diffoscope/index.html) |
| cpio | 2.13+dfsg-7.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/cpio/2.13+dfsg-7.1/buildlog.txt) | build [7291760265](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760265) <br> diff [7291760281](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760281) | [diff 7291760281](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291760281/artifacts/diffoscope/index.html) |
| e2fsprogs | 1.47.0-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/e2fsprogs/1.47.0-2/buildlog.txt) | build [7294445404](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7294445404) <br> diff [7294445420](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7294445420) | [diff 7294445420](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7294445420/artifacts/diffoscope/index.html) |
| expat | 2.5.0-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/expat/2.5.0-1/buildlog.txt) | build [7291666010](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666010) <br> diff [7291666026](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666026) | [diff 7291666026](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291666026/artifacts/diffoscope/index.html) |
| findutils | 4.9.0-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/findutils/4.9.0-4/buildlog.txt) | build [7291796464](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291796464) <br> diff [7291796494](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291796494) | [diff 7291796494](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291796494/artifacts/diffoscope/index.html) |
| gettext | 0.21-12 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/gettext/0.21-12/buildlog.txt) | build [7291666014](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666014) <br> diff [7291666027](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666027) | [diff 7291666027](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291666027/artifacts/diffoscope/index.html) |
| gnupg2 | 2.2.40-1.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/gnupg2/2.2.40-1.1/buildlog.txt) | build [7291716076](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716076) <br> diff [7291716081](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716081) | [diff 7291716081](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291716081/artifacts/diffoscope/index.html) |
| gzip | 1.12-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/gzip/1.12-1/buildlog.txt) | build [7291972345](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291972345) <br> diff [7291972354](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291972354) | [diff 7291972354](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291972354/artifacts/diffoscope/index.html) |
| hostname | 3.23+nmu1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/hostname/3.23+nmu1/buildlog.txt) | build [7291804971](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291804971) <br> diff [7291804976](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291804976) | [diff 7291804976](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291804976/artifacts/diffoscope/index.html) |
| iputils | 3:20221126-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/iputils/3:20221126-1/buildlog.txt) | build [7291701592](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701592) <br> diff [7291701599](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701599) | [diff 7291701599](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291701599/artifacts/diffoscope/index.html) |
| keyutils | 1.6.3-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/keyutils/1.6.3-2/buildlog.txt) | build [7291701591](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701591) <br> diff [7291701598](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701598) | [diff 7291701598](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291701598/artifacts/diffoscope/index.html) |
| libcap2 | 1:2.66-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/libcap2/1:2.66-4/buildlog.txt) | build [7291771527](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291771527) <br> diff [7291771534](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291771534) | [diff 7291771534](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291771534/artifacts/diffoscope/index.html) |
| libedit | 3.1-20221030-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/libedit/3.1-20221030-2/buildlog.txt) | build [7291938486](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291938486) <br> diff [7291938494](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291938494) | [diff 7291938494](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291938494/artifacts/diffoscope/index.html) |
| libgpg-error | 1.46-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/libgpg-error/1.46-1/buildlog.txt) | build [7291666005](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666005) <br> diff [7291666020](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291666020) | [diff 7291666020](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291666020/artifacts/diffoscope/index.html) |
| liblocale-gettext-perl | 1.07-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/liblocale-gettext-perl/1.07-5/buildlog.txt) | build [7301200837](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301200837) <br> diff [7301200852](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301200852) | [diff 7301200852](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7301200852/artifacts/diffoscope/index.html) |
| libselinux | 3.4-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/libselinux/3.4-1/buildlog.txt) | build [7291701589](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701589) <br> diff [7291701597](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291701597) | [diff 7291701597](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291701597/artifacts/diffoscope/index.html) |
| logrotate | 3.21.0-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/logrotate/3.21.0-1/buildlog.txt) | build [7301282554](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301282554) <br> diff [7301282565](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7301282565) | [diff 7301282565](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7301282565/artifacts/diffoscope/index.html) |
| lvm2 | 2.03.16-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/lvm2/2.03.16-2/buildlog.txt) | build [7291804973](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291804973) <br> diff [7291804978](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291804978) | [diff 7291804978](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291804978/artifacts/diffoscope/index.html) |
| mawk | 1.3.4.20200120-3.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/mawk/1.3.4.20200120-3.1/buildlog.txt) | build [7291752906](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752906) <br> diff [7291752921](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752921) | [diff 7291752921](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291752921/artifacts/diffoscope/index.html) |
| newt | 0.52.23-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/newt/0.52.23-1/buildlog.txt) | build [7291716079](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716079) <br> diff [7291716084](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291716084) | [diff 7291716084](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291716084/artifacts/diffoscope/index.html) |
| perl | 5.36.0-7+deb12u1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/perl/5.36.0-7+deb12u1/buildlog.txt) | build [7291938484](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291938484) <br> diff [7291938490](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291938490) | [diff 7291938490](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291938490/artifacts/diffoscope/index.html) |
| popt | 1.19+dfsg-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/popt/1.19+dfsg-1/buildlog.txt) | build [7291760262](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760262) <br> diff [7291760275](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760275) | [diff 7291760275](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291760275/artifacts/diffoscope/index.html) |
| popularity-contest | 1.76 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/popularity-contest/1.76/buildlog.txt) | build [7291752904](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752904) <br> diff [7291752915](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291752915) | [diff 7291752915](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291752915/artifacts/diffoscope/index.html) |
| procps | 2:4.0.2-3 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/procps/2:4.0.2-3/buildlog.txt) | build [7291796476](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291796476) <br> diff [7291796496](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291796496) | [diff 7291796496](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291796496/artifacts/diffoscope/index.html) |
| sed | 4.9-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/sed/4.9-1/buildlog.txt) | build [7291760257](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760257) <br> diff [7291760274](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291760274) | [diff 7291760274](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291760274/artifacts/diffoscope/index.html) |
| shadow | 1:4.13+dfsg1-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/shadow/1:4.13+dfsg1-1/buildlog.txt) | build [7291972344](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291972344) <br> diff [7291972353](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291972353) | [diff 7291972353](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291972353/artifacts/diffoscope/index.html) |
| zlib | 1:1.2.13.dfsg-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/blob/main/logs/zlib/1:1.2.13.dfsg-1/buildlog.txt) | build [7291991375](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291991375) <br> diff [7291991384](https://gitlab.com/debdistutils/reproduce/debian-bookworm-amd64/-/jobs/7291991384) | [diff 7291991384](https://debdistutils.gitlab.io/-/reproduce/debian-bookworm-amd64/-/jobs/7291991384/artifacts/diffoscope/index.html) |

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
`Date` field in the respectively `Release` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**Mon Jul  8 20:22:01 UTC 2024**.

| Archive/Suite | Timestamp |
| ------------- | --------- |
| debian/bookworm | Sat, 29 Jun 2024 09:06:14 UTC |
| debian/bookworm-updates | Mon, 08 Jul 2024 20:22:01 UTC |
| debian-security/bookworm-security | Sun, 07 Jul 2024 12:12:17 UTC |

## License

The content of this repository is automatically generated by
[debdistrebuild](https://gitlab.com/debdistutils/debdistrebuild),
which is published under the
[AGPLv3+](https://www.gnu.org/licenses/agpl-3.0.en.html) and to the
extent the outputs are copyrightable they are released under the same
license.

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).

